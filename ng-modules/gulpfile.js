'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var argv = require('yargs').argv;

//tasks to build resources
var buildScript = require('./tasks/build-script');
var buildStyle = require('./tasks/build-style');
var watchModule = require('./tasks/watch');
var copyImg = require('./tasks/copy-img');
var copyTpl = require('./tasks/copy-tpl');

//build single module
if (argv.module) {
    console.log('Building specific module:' + argv.module);

    gulp.task('build:style', buildStyle);
    gulp.task('build:script', buildScript);
    gulp.task('copy:img', copyImg);
    gulp.task('copy:tpl', copyTpl);

    gulp.task('publish', [
        'build:script',
        'build:style',
        'copy:img',
        'copy:tpl' 
    ]);

    gulp.task('watch', watchModule);
//batch build
}else {
    console.log('Batch building modules not implemented yet!');
}
