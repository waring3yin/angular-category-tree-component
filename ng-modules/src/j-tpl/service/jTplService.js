'use strict';

var config = require('../../../config.json');

function factory() {
    return new jTplService(config);
}

function jTplService(config) {
    this.config = config;
}

jTplService.prototype.setCurrentModule = function(moduleName) {
    this.currentModule = moduleName;
};

jTplService.prototype.getTemplateUrl = function(moduleName, tpl) {
    var currentModule = this.currentModule || moduleName,
        url;

    url = [
        this.config.tpl.basepath,
        '/',
        currentModule,
        '/tpl/',
        tpl
    ].join(''); 

    if (this.config.debug) {
        var d = new Date();

        url += '?' + d.getTime();
    }

    return url;
};

module.exports = factory;

