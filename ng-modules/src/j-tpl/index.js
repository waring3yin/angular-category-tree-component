var angular = require('angular');
var serviceJTpl = require('./service/jTplService');

var moduleJTpl = angular.module('j-tpl', []);

moduleJTpl.factory('jTplService', serviceJTpl);

module.exports = moduleJTpl;