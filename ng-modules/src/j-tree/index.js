var angular = require('angular');
var compTreeModel = require('./component/jTreeModel');

var moduleJtree = angular.module('j-tree', []);

moduleJtree.directive('treeModel', compTreeModel);

module.exports = moduleJtree;