'use strict';

function jCategoryTree($scope) {
    var ctrl = this;

    ctrl.showReset = false;

    $scope.$watch('jCtree.currentNode', function(newNode, oldNode) {
        if($scope.jCtree && angular.isObject($scope.jCtree.currentNode)) {
            ctrl.showReset = true;

            ctrl.onChangeNode({categoryId: newNode.id});
        }
    }, false);
}

jCategoryTree.$inject = ['$scope'];

function getTemplateUrl(jTplService) {
    return jTplService.getTemplateUrl('j-category-tree', 'tree.html');
}

getTemplateUrl.$inject = ['jTplService'];

module.exports = {
    bindings: {
        treenodes: '<',
        onChangeNode: '&'
    },
    controller: jCategoryTree,
    templateUrl: getTemplateUrl
};

