'use strict';

//node modules
var angular = require('angular'),
    ngSanitize = require('angular-sanitize');

//j modules
var jtree = require('../j-tree');
var jtpl = require('../j-tpl');

//current module
var moduleJCategoryTree = angular.module(
    'j-category-tree', 
    [
        jtree.name, 
        jtpl.name,
        ngSanitize
    ]
);

//module component
var componentJCategoryTree = require('./component/jCategoryTree');
moduleJCategoryTree.component('jCategoryTree', componentJCategoryTree);

module.exports = moduleJCategoryTree;