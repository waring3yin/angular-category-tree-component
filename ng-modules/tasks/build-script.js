'use strict';

//dependent modules
var gulp = require('gulp');
var argv = require('yargs').argv;
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require('gulp-util');
var watchify = require('watchify');

//global build configrations
var config = require('../config.json'); 

function build() {
    // set up the browserify instance on a task basis
    var b = browserify(['./src/' + argv.module + '/index.js'], {
        debug: config.debug,
        plugin: [watchify]
    }),
    dest = config.dest + '/' + argv.module + '/js';

    b.on('update', function() {
        bundle(b, dest);
        
        console.log('Rebuild the script automatically.');        
    });

    return bundle(b, dest);
}

function bundle(b, dest) {
    if (config.debug) {
        return b.bundle()
        .pipe(source('module.js'))
        .pipe(buffer())
        .on('error', gutil.log)
        .pipe(gulp.dest(dest));
    }else {
        return b.bundle()
        .pipe(source('module.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest(dest));
    }
}

module.exports = build;