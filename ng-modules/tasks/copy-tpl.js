'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    argv = require('yargs').argv,
    lodash = require('lodash'),
    fs = require('fs');

//configrations
var configGlobal = require('../config.json'),
    configModule,
    srcFold = './src/' + argv.module + '/tpl',
    destFold = configGlobal.dest + '/' + argv.module + '/tpl';

if (fs.existsSync('./src/' + argv.module + '/module.json')) {
    configModule = require('../src/' + argv.module + '/module.json');
}

//copy
var tpls = [];

if (fs.existsSync(srcFold)) {
   tpls.push(srcFold + '/*'); 
   tpls.push(srcFold + '/**/*'); 
}
//append tpl of dependent modules
if (configModule) {
    lodash.forEach(
        configModule.dependencies, 
        function(dependentModule) {
            tpls.push('./src/' + dependentModule + '/tpl/*');
            tpls.push('./src/' + dependentModule + '/tpl/**');
        }
    );
}

function copyTpl() {
    return gulp.src(tpls).pipe(gulp.dest(destFold));
}

module.exports = copyTpl;
