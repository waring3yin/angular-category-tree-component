'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    argv = require('yargs').argv,
    lodash = require('lodash'),
    fs = require('fs');

//configrations
var configModule;

if (fs.existsSync('./src/' + argv.module + '/module.json')) {
    configModule = require('../src/' + argv.module + '/module.json');
}

//watch resources
var cssWatches = [],
    imgWatches = [],
    tplWatches = [];

appendWatches(argv.module);

//append resources to watch within dependent modules
if (configModule) {
    lodash.forEach(
        configModule.dependencies, 
        function(dependentModule) {
           appendWatches(dependentModule);
        }
    );
}

//append watches
function appendWatches(moduleName) {
    appendCssWatches(moduleName);
    appendImgWatches(moduleName);
    appendTplWatches(moduleName); 
}

function appendCssWatches(moduleName) {
    cssWatches.push('./src/' + moduleName + '/less/**/*.less');
    cssWatches.push('./src/' + moduleName + '/less/main.less');

    cssWatches.push('./src/' + moduleName + '/module.json');
}

function appendImgWatches(moduleName) {
    imgWatches.push('./src/' + moduleName + '/img/**/*');
    imgWatches.push('./src/' + moduleName + '/img/*');
}

function appendTplWatches(moduleName) {
    tplWatches.push('./src/' + moduleName + '/tpl/*'); 
}

function watch() {
    //css
    gulp.watch(cssWatches, ['build:style']);

    //img
    gulp.watch(imgWatches, ['copy:img']);

    //tpl
    gulp.watch(tplWatches, ['copy:tpl']);

    return true;
}

module.exports = watch;
