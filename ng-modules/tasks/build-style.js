'use strict';

//node modules
var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    argv = require('yargs').argv,
    lodash = require('lodash'),
    fs = require('fs');

//configrations
var configGlobal = require('../config.json'),
    configModule;

if (fs.existsSync('./src/' + argv.module + '/module.json')) {
    configModule = require('../src/' + argv.module + '/module.json');
}

//build
var lesses = [],
    commonLess = [];

if (fs.existsSync('./src/' + argv.module + '/less/main.less')) {
    lesses.push('./src/' + argv.module + '/less/main.less');
}

//append css of dependent modules
if (configModule) {
    //common less
    if (configModule.commonless) {
       commonLess = lodash.union(commonLess, configModule.commonless);
    }

    //private css of dependent module
    if (configModule.dependencies) {
        lodash.forEach(
            configModule.dependencies, 
            function(dependentModule) {
                if (fs.existsSync('./src/' + dependentModule + '/less/main.less')) {
                    lesses.push('./src/' + dependentModule + '/less/main.less');
                }

                if (fs.existsSync('./src/' + dependentModule + '/module.json')) {
                    var moduleConfig = require('../src/' + dependentModule + '/module.json');

                    //common less
                    if (moduleConfig.commonless) {
                       commonLess = lodash.union(commonLess, moduleConfig.commonless);
                    }
                }
            }
        );
    }
}

if (commonLess.length) {
    lodash.forEach(commonLess, function(lessItem) {
        lesses.push('./src/j-common/less/' + lessItem);
    });
}

function build() {
    if (lesses.length) {
        return gulp.src(lesses)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.less({strictMath: true}))
        .pipe(plugins.cleanCss({keepSpecialComments : 0}))
        .pipe(plugins.concat('module.css'))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(configGlobal.dest + '/' + argv.module + '/css')); 
    }

    return true;

}

module.exports = build;