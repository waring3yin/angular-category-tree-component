'use strict';

var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    argv = require('yargs').argv,
    lodash = require('lodash'),
    fs = require('fs');

//configrations
var configGlobal = require('../config.json'),
    configModule,
    srcImgFold = './src/' + argv.module + '/img',
    destImgFold = configGlobal.dest + '/' + argv.module + '/img';

if (fs.existsSync('./src/' + argv.module + '/module.json')) {
    configModule = require('../src/' + argv.module + '/module.json');
}

//copy
var imgs = [];

if (fs.existsSync(srcImgFold)) {
   imgs.push(srcImgFold + '/*'); 
   imgs.push(srcImgFold + '/**/*'); 
}

//append img of dependent modules
if (configModule) {
    lodash.forEach(
        configModule.dependencies, 
        function(dependentModule) {
            imgs.push('./src/' + dependentModule + '/img/*');
            imgs.push('./src/' + dependentModule + '/img/**');
        }
    );
}

function copyImg() {
    return gulp.src(imgs).pipe(gulp.dest(destImgFold));
}

module.exports = copyImg;
